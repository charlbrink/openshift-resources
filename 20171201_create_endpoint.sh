#!/bin/bash

if [[ $# -eq 0 ]]; then
    echo 'Please provide the environment as a parameter - local'
    exit 1
fi

ENV=${1:-dev}

file="env/${ENV}.properties"

if [[ -f "$file" ]]; then
  echo "$file found."

  while IFS='=' read -r key value
  do
    key=$(echo $key | tr '.' '_')
    eval "${key}='${value}'"
  done < "$file"

else
  echo "$file not found."
  exit 1  
fi

oc project ${PROJECT_NAME}

if [ $? -ne 0 ]; then
    echo "Please use the oc login command to log in to ${PROJECT_NAME}"
    exit 1
fi

echo PROJECT_NAME=${PROJECT_NAME}
echo ZONE=${ZONE}
echo PRIMARY=${MY_HOST_PRIMARY}
echo SECONDARY=${MY_HOST_SECONDARY}
echo PORT=${MY_PORT}

cat templates/endpoint-service-template.json | oc process -p MY_PORT=${MY_PORT} -p ZONE=${ZONE} -f - | oc create -n ${PROJECT_NAME} -f - -o name
cat templates/endpoint-template.json | oc process -p MY_HOST_PRIMARY=${MY_HOST_PRIMARY} -p MY_HOST_SECONDARY=${MY_HOST_SECONDARY} -p MY_PORT=${MY_PORT} -p ZONE=${ZONE} -f - | oc create -n ${PROJECT_NAME} -f - -o name
