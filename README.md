# Openshift Resources

## Usage

* First log in and select the correct project
<pre>
oc cluster up
oc login https://127.0.0.1:8443 --token=&lt;token&gt;
oc project myproject
</pre>

* Run the required script passing the environment as a parameter local
<pre>
20171201create_endpoint.sh dev
</pre>

## Example OC commands

Create a resource from a file: <pre>oc create -f filename.json</pre>

Export a resource to a template: <pre>oc export <object_type> <object_name> -o json --as-template='<template_name>'</pre>

Create a resource from a template: <pre>oc process -f <filename> | oc create -f -</pre>